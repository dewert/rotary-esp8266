#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h>
#include <PubSubClient.h>
#include <string>
#include <cstring>

#include <SD.h>
#include <ESP8266HTTPClient.h>
#include <AudioFileSourceSPIFFS.h>
#include <AudioGeneratorWAV.h>
#include <AudioOutputI2SNoDAC.h>

#include "Arduino.h"
#include "SoftwareSerial.h"

const int PHONE_PORT = D1;
const ulong MAXIMUM_PULSE_LENGTH = 100;
const ulong MINIMUM_PULSE_LENGTH = 20;
const ulong READ_LOOP_TIME = 10;
const uint16 LOOP_TIME = 1000;

WiFiClient wifiClient;
WiFiServer server(80);
PubSubClient mqtt(wifiClient);

struct MusicPlay {
  AudioGeneratorWAV *wav;
  AudioFileSourceSPIFFS *file;
  AudioOutputI2SNoDAC *out;
};
MusicPlay music;

int lastDialled;

struct MQTTDetails {
  String server;
  uint16_t port;
  String topic;
};

MQTTDetails mqttDetails;


bool configurePhone();
bool configureWifi();
bool configureWebserver();
bool connectMqtt();

bool playHoldMusic() {
  delete music.file;
  delete music.out;
  delete music.wav;

  music.file = new AudioFileSourceSPIFFS("/holdmusic.wav");
  music.out = new AudioOutputI2SNoDAC();
  music.wav = new AudioGeneratorWAV();
  music.wav->begin(music.file, music.out);

  return true;
}

int handleChange(int state) {
  if (state == HIGH) {
    return 1;
  } else {
    return 0;
  }
}

int parseDigit(int phoneDigit) {
  if (digitalRead(PHONE_PORT) == HIGH) {
    // phone is on the hook - not a real digit
    return -1;
  }

  if (phoneDigit > 0) {
    if (phoneDigit > 9) {
      phoneDigit = 0;
    }
    return phoneDigit;
  }
  return -1;
}

void handleWebServer() {
  WiFiClient client = server.available();
  if (client) {
    Serial.println("\n[Client connected]");
    while (client.connected()) {
      if (client.available()) {
        String line = client.readStringUntil('\r');
        Serial.print(line);
        // wait for end of client's request, that is marked with an empty line
        if (line.length() == 1 && line[0] == '\n')
        {
          
          String header =
            String("HTTP/1.1 200 OK\r\n") +
                   "Content-Type: application/json\r\n" +
                   "Connection: close\r\n" +  // the connection will be closed after completion of the response
                   "\r\n";
          client.println(header);
          String body = 
            String("{\n") +
                   "  \"timestamp\": " + millis() + ",\n" +
                   "  \"mqttServer\": \"" + mqttDetails.server + ":" + mqttDetails.port + "\",\n" +
                   "  \"mqttTopic\": \"" + mqttDetails.topic + "\",\n" +
                   "  \"lastDialled\": " + lastDialled + ",\n" +
                   "  \"wifiAP\": \"" + WiFi.SSID() + "\"\n" +
                   "}\n"; 
          client.println(body);
          client.printf("\r\n");
          break;
        }
      }
    }
  }
}

void loop(void) {
  static int dialled = -1;
  static int digit = 0;
  static int state;
  static int oldState = digitalRead(PHONE_PORT);

  static ulong lastChangedTime = millis();

  handleWebServer();
  state = digitalRead(PHONE_PORT);

  if (state == LOW) {
    if (!music.wav || !(music.wav->isRunning())) {
      playHoldMusic();
    } else {
      if (!music.wav->loop()) music.wav->stop(); 
    }
  }

  if (state != oldState) {
    digit += handleChange(state);
    lastChangedTime = millis();
  }
  oldState = state;
  delay(READ_LOOP_TIME);
  if (millis() - lastChangedTime > LOOP_TIME) {
    dialled = parseDigit(digit);
    if (dialled > -1) {
      Serial.printf("Dialled %d\n", dialled);
      lastDialled = dialled;
      char dialChar[2];
      itoa(lastDialled, dialChar, 10);
      connectMqtt();
      mqtt.publish(mqttDetails.topic.c_str(), dialChar);
    }
    digit = 0;
  };
}

void setup() {
  Serial.begin(9600);
  delay(10);
  Serial.println('\n');
  
  Serial.println("Intellectual Home 1.0");

  if (!configurePhone()) {
    return;
  }

  if (!SPIFFS.begin()) {
    Serial.println("Error initializing filesystem");
    return;
  }

  if (!configureWifi()) {
    Serial.println("Error initializing network");
    return;
  }
}

bool configurePhone() {
  lastDialled = -1;
  pinMode(PHONE_PORT, INPUT_PULLUP);

  return true;
}

bool connectMqtt() {
  if (mqtt.connected()) {
    return true;
  }
  if (mqtt.connect("rotary-phone")) {
    Serial.println("Successfully connected to MQTT");
    return true;
  }
  return false;
}

bool configureWifi() {
  File f = SPIFFS.open("/wifi_creds", "r");
  if (!f) {
    Serial.println("Could not read wifi credentials");
    return false;
  }

  String ap = f.readStringUntil('\n');
  String key = f.readStringUntil('\n');
  mqttDetails.server = f.readStringUntil('\n');
  mqttDetails.port = atoi(f.readStringUntil('\n').c_str());
  mqttDetails.topic = f.readStringUntil('\n');

  WiFi.begin(ap.c_str(), key.c_str());

  Serial.println("Connecting ...");

  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    Serial.print('.');
  }
  Serial.println('\n');

  Serial.print("Connected to ");
  Serial.println(WiFi.SSID());
  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());

  server.begin();

  mqtt.setServer(mqttDetails.server.c_str(), mqttDetails.port);
  return connectMqtt();
}
